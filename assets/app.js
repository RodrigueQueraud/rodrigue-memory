



function startTimer() {
	let timer = new Date().getTime()+ 60000;

	let intervalle = setInterval(function() {

	  let now = new Date().getTime();
	    
	  let distance = timer - now;
	   
	  let seconds = Math.floor((distance % (1000 * 60)) / 1000);
	    
	  document.getElementById("compteur").innerHTML =seconds + "s ";
	    
	  if (distance < 0) {
	    clearInterval(intervalle);
	    alert('Game Over');
	  }
	}, 1000);
}
	
document.getElementById('start').onclick = startTimer;




const cards = document.querySelectorAll('.card');

function flipCard() {
  this.classList.toggle('flip');
}

cards.forEach(card => card.addEventListener('click', flipCard));